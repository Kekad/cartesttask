<?php

use Car\Battery;
use Car\Car;
use Car\Door;
use Car\Exceptions\ExceptionNotDetail;
use Car\Tyre;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

class CarTest extends TestCase
{

    /**
     * Passing a list of all details and check painting damage
     * @return Car
     * @throws ExceptionNotDetail
     */
    public function testisPaintingDamaged() {
        $car = new Car([
            new Tyre(),
            new Battery(),
            new Door(),
        ]);

        $this->assertFalse($car->isPaintingDamaged(), "Sorry but painting damaged");
        return $car;
    }

    /**
     * This test check if detail is broken
     * @depends testisPaintingDamaged
     * @param Car $car
     */
    public function testisDetailBroken(Car $car) {
        $this->assertFalse($car->isBroken(), "Sorry but you car damaged [Detail is broken]");
    }

    /**
     * This test check if painted details have a deep scratches in car's body paint
     * @depends testisPaintingDamaged
     * @param Car $car
     */
    public function testisPaintingDamageHigh(Car $car) {
        $this->assertFalse($car->isPaintingDamagedHigh(), "Sorry but you car damaged [Painting has a high damage]");
    }

    /**
     * This test check if battery current greater than 12 V
     * @depends testisPaintingDamaged
     * @param Car $car
     * @throws Exception
     */
    public function testisCurrent(Car $car) {
        $expectedCurrent = 12;
        $this->assertGreaterThan($expectedCurrent, $car->getDetail(Battery::class)->getCurrent(), "Sorry but you battery current grater than $expectedCurrent V");

    }

    /**
     * Printing a debug message in console
     * @param $msg
     */
    public function printDebugMsg($msg) {
        fwrite(STDERR, print_r($msg, TRUE));
    }

}