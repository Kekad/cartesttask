<?php

namespace Car\Exceptions;

use Exception;

/**
 * Class ExceptionNotDetail
 * @package Car
 */
class ExceptionNotDetail extends Exception
{
    /**
     * @var string
     */
    protected $message = 'Not a car detail';
}