<?php


namespace Car;


/**
 * Interface CarDetailPaintableInterface
 * @package Car
 */
interface CarDetailPaintableInterface
{
    /**
     * Check if painting is damaged
     * @return bool
     */
    public function isPaintingDamaged(): bool;

    /**
     * Check if painting has a depp scratch
     * @return bool
     */
    public function isPaintingDamageHigh(): bool;
}