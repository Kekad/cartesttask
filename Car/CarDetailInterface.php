<?php
namespace Car;

/**
 * Interface CarDetailInterface
 * @package Car
 */
interface CarDetailInterface
{
    /**
     * Check if detail is broken
     * @return bool
     */
    public function isBroken(): bool;
}