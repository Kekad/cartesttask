<?php

namespace Car;


use Car\Exceptions\ExceptionNotDetail;
use Exception;


/**
 * Class Car
 * @package Car
 */
class Car
{

    /**
     * @var  []CarDetailInterface
     */
    private $details;

    /**
     * Create new car with passed {@link $details}
     * @param array $details
     * @throws ExceptionNotDetail
     */
    public function __construct(array $details) {

        foreach ($details as $detail) {
            if (!$detail instanceof CarDetailInterface) {
                throw new ExceptionNotDetail();
            }
        }
        $this->details = $details;
    }

    /**
     * check car is broken
     * @return bool
     */
    public function isBroken(): bool {
        foreach ($this->details as $detail) {

            if ($detail->isBroken()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if painting is damaged.
     * @return bool
     */
    public function isPaintingDamaged(): bool {
        foreach ($this->details as $detail) {
            if ($detail instanceof CarDetailPaintableInterface && $detail->isPaintingDamaged()) {
                return true;
            }
        }
        return false;
    }


    /**
     * Check if a car has a deep scratches
     * @return bool
     */
    public function isPaintingDamagedHigh() {
        foreach ($this->details as $detail) {
            if ($detail instanceof CarDetailPaintableInterface && $detail->isPaintingDamageHigh()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get detail by its class
     * @param $detail
     * @return mixed
     * @throws Exception
     */
    public function getDetail($detail) {
        foreach ($this->details as $key => $object) {
            if ($object instanceof $detail) {
                return $object;
            }
        }
        throw new Exception("Detail not found");
    }
}