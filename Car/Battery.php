<?php


namespace Car;


/**
 * Class Battery
 * @package Car
 */
class Battery implements CarDetailInterface
{

    /**
     * Battery current
     * @var int
     */
    private $current;
    /**
     * battery broken status
     * @var bool
     */
    private $isBroken;

    /**
     * Battery constructor.
     */
    public function __construct() {
        $this->current = 13;
        $this->isBroken = false;
    }

    /**
     * Getter for battery's current
     * @return int
     */
    public function getCurrent(): int {
        return $this->current;
    }

    /**
     * Setter for battery's current
     * @param int $current
     */
    public function setCurrent(int $current): void {
        $this->current = $current;
    }

    /**
     * Get battery broken status
     * @return bool
     */
    public function isBroken(): bool {
        return $this->isBroken;
    }
}