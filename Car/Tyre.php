<?php


namespace Car;


/**
 * Class Tyre
 * @package Car
 */
class Tyre implements CarDetailInterface
{

    /**
     * Tyre broken status
     * @var bool
     */
    private $isBroken;

    /**
     * Tyre constructor.
     */
    public function __construct() {
        $this->isBroken = false;
    }

    /**
     * Get broken status
     * @return bool
     */
    public function isBroken(): bool {
        return $this->isBroken;
    }

}