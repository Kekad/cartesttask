<?php


namespace Car;


/**
 * Class Door
 * @package Car
 */
class Door implements CarDetailInterface, CarDetailPaintableInterface
{

    /**
     * Door broken status
     * @var bool
     */
    private $isBroken;
    /**
     * Painting  status
     * @var bool
     */
    private $isPaintingDamaged;
    /**
     * Painting scratched status
     * @var bool
     */
    private $isPaintingDamageHigh;

    /**
     * Door constructor.
     */
    public function __construct() {
        $this->isBroken = false;
        $this->isPaintingDamaged = false;
        $this->isPaintingDamageHigh = false;
    }

    /**
     * Get Door broken status
     * @return bool
     */
    public function isBroken(): bool {
        return $this->isBroken;
    }

    /**
     * Get Door painting status
     * @return bool
     */
    public function isPaintingDamaged(): bool {
        return $this->isPaintingDamaged;
    }

    /**
     * Get Door painting scratched status
     * @return bool
     */
    public function isPaintingDamageHigh(): bool {
        return $this->isPaintingDamageHigh;
    }
}